<?php

/**
 * @file
 * Configuration options for JAIL Formatter settings
 */

/**
 * Menu callback; Settings administration.
 */
function jail_formatter_admin_settings() {
  $form['jail_formatter_users'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Lazy Load User Images'),
    '#description'   => t('If enabled, user pictures will "lazy load"'),
    '#default_value' => variable_get('jail_formatter_users', 0),
  );

  $form['jail_formatter_place'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Place Holder Image'),
    '#description'   => t('Path to place holder image'),
    '#default_value' => variable_get('jail_formatter_place', drupal_get_path('module', 'jail_formatter') . '/white.gif'),
  );
  $form['jail_formatter_loader'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Loader Image'),
    '#description'   => t('Path to loader image'),
    '#default_value' => variable_get('jail_formatter_loader', drupal_get_path('module', 'jail_formatter') . '/white.gif'),
  );
  $form['jail_formatter_effect'] = array(
    '#type'           => 'select',
    '#title'          => t('User Picture Effect'),
    '#description'    => t('Select the effect to use for user pictures'),
    '#default_value'  => variable_get('jail_formatter_effect','show'),
    '#options'        => array(
      'show'  => 'None',
      'fadeIn' => 'Fade in',
      'slideDown'  => 'Slide down',
    ),
  );
  return system_settings_form($form);
}
